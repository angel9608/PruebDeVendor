<?php

use App\User;
use App\Profession;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $professionId= Profession::where('profession','Desarrollador back-end')->value('id');

        User::create([
            'name'=>'Angel Mora',
            'email'=>'angel@gmail.com',
            'password'=>bcrypt('laravel'),
            'profession_id'=>$professionId,
            'is_admin'=>true
        ]);

        User::create([
            'name'=>'Juan Angel Mora',
            'email'=>'angel2@gmail.com',
            'password'=>bcrypt('laravel'),
            'profession_id'=>$professionId
        ]);

        User::create([
            'name'=>'Juan Angel Mora',
            'email'=>'angel3@gmail.com',
            'password'=>bcrypt('laravel'),
            'profession_id'=> null
        ]);
    }
}
