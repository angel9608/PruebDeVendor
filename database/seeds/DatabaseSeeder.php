<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->truncateTable([
            'users',
            'my_professions'
        ]);

        // dd(UserSeeder::class);
        $this->call(professionalSeeder::class);
        $this->call(UserSeeder::class);
    }

    
    protected function truncateTable(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach($tables as $table){
            DB::table($table)->truncate();
        }
    }

}
