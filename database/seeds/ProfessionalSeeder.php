<?php


use App\Profession;
use Illuminate\Database\Seeder;
use Illuminate\support\Facades\DB;

class professionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Profession::create([
            'profession'=>'Desarrollador back-end'
        ]);
        Profession::create([
            'profession'=>'Desarrollador front-end'
        ]);
        Profession::create([
            'profession'=>'Diseñador Web'
        ]);

        // DB::table('profession')->insert([
        //     'id'=>'1'
        // ]);
        // DB::table('profession')->insert([
        //     'professional_id'=>'2'
        // ]);
        // DB::table('profession')->insert([
        //     'profession'=>'Sistemas'
        // ]);
    }
}
