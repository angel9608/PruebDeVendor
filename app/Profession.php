<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $table='my_professions';
    protected $fillable=['profession'];

    public function users(){
        return $this->hasMany(User::class);
    }
}
